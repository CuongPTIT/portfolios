import 'dart:io';

import 'package:flutter/material.dart';

extension ScreenSize on BuildContext {
  double sizeWith() {
    return MediaQuery.of(this).size.width;
  }

  double sizeHeight() {
    return MediaQuery.of(this).size.height;
  }

  MediaQueryData mediaQueryData() {
    return MediaQuery.of(this);
  }
}

extension DevicesType on BuildContext {
  bool isIphoneX() {
   return  (Platform.isIOS && this.sizeHeight() > 812);
  }
}
