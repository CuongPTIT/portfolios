class Constants {
  static const double tsMini = 12;
  static const double tsNormal = 14;
  static const double tsTitle = 16;
  static const double tsBig = 18;
  static const double tsTitleBig = 25;

  static const String icFb = 'assets/icons/ic_fb.png';
  static const String icBlog = 'assets/icons/ic_blog.ico';
  static const String imgAvatar = 'assets/images/img_avt.jpg';
  static const String imgTat = 'assets/images/img_tatsuno.png';
  static const String imgPi = 'assets/images/img_piranet.png';
  static const String imgDiet = 'assets/images/img_dietto.png';
  static const String imgDIY = 'assets/images/img_diy.png';
  static const String imgVM = 'assets/images/img_van_mau.png';
  static const String imgWeather = 'assets/images/img_weather.png';

  static const String resume = 'Resume';
  static const String send = 'Gửi';
  static const String project = 'Dự án';
  static const String contact = 'Liên hệ';
  static const String hello = 'Xin chào,tôi là ';
  static const String surName = 'Tuấn Cường';
  static const String chamThan = '!';
  static const String phone = 'Điện thoại';
  static const String numberPhone = '0355079292';
  static const String email = 'Email';
  static const String mail = 'thachtuancuong@gmail.com';
  static const String followMe = 'Theo dõi tôi';
  static const String copyright = '© By Cường Két';
  static const String createdBy = 'Created with Flutter';
  static const String home = 'Trang chủ';
  static const String experience = 'Kinh nghiệm làm việc';
  static const String education = 'Giáo dục';
  static const String skill = 'Kĩ năng';
  static const String reference = 'Tham chiếu';
  static const String fullName = 'Thạch Tuấn Cường';
  static const String birthday = '01/11/1998';
  static const String address = 'Hà Đông - Hà Nội';
  static const String country = 'Bắc Giang';
  static const String textName = 'Tên';
  static const String textBirthday = 'Ngày sinh';
  static const String textAddress = 'Địa chỉ';
  static const String textCountry = 'Quê quán';
  static const String ho = 'Họ';
  static const String subject = 'Chủ đề';
  static const String content = 'Nội dung';

  static const String pirago = 'Công ty TNHH Pirago';
  static const String vccorp = 'Công ty cổ phần VCCorp';
  static const String vnpt = 'VNPT Technology';
  static const String devpro = 'Công ty Cổ Phần DevPro Việt Nam';
  static const String ptit = 'Học viện Công nghệ BCVT';
  static const String languages = 'Ngôn ngữ lập trình';
  static const String databases = 'Cơ sở dữ liệu';
  static const String english = 'Tiếng anh';
  static const String otherSkills = 'Các kĩ năng khác';
  static const String personReference = 'Đinh Quốc Tuấn';
  static const String desReference = 'Leader Mobile Team - Công ty Pirago\n\n0989618519\n\ntuandq@gmail.com';
  static const String timePirago = '09/2020 - nay';
  static const String timeVccorp = '06/2018 - 09/2018';
  static const String timeVNPT = '07/2020 - 09/2020';
  static const String timePTIT = '2016-2021';
  static const String timeDevPro = '06/2017 - 09/2017';
  static const String timeBlank = '';
  static const String desPirago =
      'Lập trình viên mobile:\n\nPhát triển, bảo trì ứng dụng di dộng bằng ngôn ngữ Java, Kotlin, Flutter';
  static const String desVNPT =
      'Thực tập sinh:\n\nNghiên cứu, bảo trì ứng dụng di dộng bằng ngôn ngữ Java';
  static const String desVccorp =
      'Thực tập sinh:\n\nNghiên cứu, phát triển ứng dụng di dộng bằng ngôn ngữ Java';
  static const String desPTIT = 'Sinh viên ngành ATTT - Bằng kỹ sư\n\nGPA: 2.5';
  static const String desDevPro =
      'Học viên\n\nLớp phát triển ứng dụng di động bằng Java';
  static const String desLanguages = 'Java, Kotlin, Dart';
  static const String desDB = 'Sql, Sqlite, Hive, Google FireStore';
  static const String desEnglish = 'Toeic 700\n\nĐọc hiểu tiếng anh tốt';
  static const String desOtherSkills = 'Git, Github, GitLab\n\nKĩ năng giải quyết vấn đề bằng google\n\nKĩ năng làm việc nhóm tốt cũng như giải quyết vấn đề độc lập\n\nTư duy lập trình tốt';

  static const String nameProject = 'Tên dự án: ';
  static const String description = 'Mô tả: ';
  static const String responsibility = 'Trách nhiệm: ';
  static const String teamSize = 'Team: ';
  static const String technology = 'Công nghệ: ';
  static const String link = 'Link: ';

  static const String tatsuno = 'Tatsunoshi AR';
  static const String desTat =
      'Ứng dụng hướng dẫn khách du lịch thăm quan thành phố Tatsuno, tái hiện hình ảnh các di tích';
  static const String sizeTat = '3 người';
  static const String resTat =
      'Phát triển các chức năng liên quan đến google map, camera, xử lí animation';
  static const String techTat = 'Flutter, Google Map, HiveDB, Dio, GetX';
  static const String linkTat = '';

  static const String piranet = 'Piranet';
  static const String desPi = 'Ứng dụng nội bộ công ty Pirago';
  static const String sizePi = '4-8 người';
  static const String resPi = 'Phát triển các tính năng của ứng dụng';
  static const String techPi = 'Flutter, Dio, Flutter BloC';
  static const String linkPi = '';

  static const String diy = 'Japan DIY HomeCenter Show 2021';
  static const String desDIY = 'Ứng dụng dành cho hội chợ, triển lãm';
  static const String sizeDIY = '5 người';
  static const String resDIY =
      'Phát triển các chức năng của ứng dụng theo yêu cầu khách hàng';
  static const String techDIY = 'Flutter, Flutter BloC, Dio';
  static const String linkDIY = '';

  static const String dietto = '1 minute diet';
  static const String desDiet = 'Mạng xã hội dành cho người giảm cân';
  static const String sizeDiet = '8 người';
  static const String resDiet =
      'Bảo trì ứng dụng, phát triển một vài chức năng.';
  static const String techDiet = 'Kotlin, MVP';
  static const String linkDiet = '';

  static const String vanMau = 'Văn mẫu 4.0';
  static const String desVM =
      'Ứng dụng lưu trữ các bài văn mẫu được sử dụng bởi giới trẻ trên FB';
  static const String sizePersonal = '1 người';
  static const String resVM = 'Lên ý tưởng, thiết kế, phát triển ứng dụng';
  static const String techVM = 'Flutter, Firebase, HiveDB';
  static const String linkVM = 'https://play.google.com/store/apps/details?id=com.ket.vanmau40\nhttps://apps.apple.com/vn/app/v%C4%83n-m%E1%BA%ABu-4-0/id1581048395';

  static const String weatherForecast = 'VN Weather Forecast';
  static const String desWeather = 'Ứng dụng dự báo thời tiết';
  static const String resWeather = 'Lên ý tưởng, thiết kế, phát triển ứng dụng';
  static const String techWeather = 'Java, Sqlite, Retrofit';
  static const String linkWeather = 'https://github.com/CuongPTIT/VnWeatherApp_Stored';

  static const String urlFb = 'https://www.facebook.com/cuong.ket.bg/';
  static const String urlBlog = 'https://blog.pirago.vn/author/cuongket/';
}
