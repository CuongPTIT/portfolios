import 'dart:ui';

class Palette {
  //file color
  static const Color grayBorderFocused = Color(0xFF7C7C7C);
  static const Color grayBorder = Color(0xFFd1d1d1);
  static const Color grayButton = Color(0xFF989898);
  static const Color grayButtonBack = Color(0xFFa5a5a5);
  static const Color grayBackground = Color(0xFFdfdfdf);
  static const Color grayText = Color(0x209e9e9e);
  static const Color white = Color(0xFFFFFFFF);
  static const Color blue = Color(0xFF274CB9);
  static const Color blueTrans = Color(0x60274CB9);
  static const Color blueBackground = Color(0xFFd8ecfa);

  static const Color transparent = Color(0x00274CB9);
  static const Color transparentItem = Color(0x803d3f6d);
  static const Color transparentWhite = Color(0xb7ffffff);

  static const Color black = Color(0xFF000000);
  static const Color textDefault = Color(0xFF333333);

  static const Color red = Color(0xFFE40212);
  static const Color colorAppbar = Color(0xFFe30b17);
  static const Color colorAppbarTran = Color(0x405339CD);
  static const Color colorBtn = Color(0xFF5339CD);
  static const Color orangeText = Color(0xFFFF6B00);
  static const Color colorCamera = Color(0xFFFFE6DE);
  static const Color colorButton = Color(0xFF865D28);
  static const Color colorCheckBox = Color(0xFF4A452A);
  static const Color bgCheckBox = Color(0xFFC4BD97);
  static const Color borderCheckBox = Color(0xFF395E8B);

  static const Color blueGrey = Color(0x50D2D2FF);
  static const Color colorShopping = Color(0xff13d38e);
  static const Color colorBaby = Color(0xfff8b250);
  static const Color colorCar = Color(0xff0293ee);
}
