import 'dart:async';

import 'package:flutter/material.dart';
import 'package:portfolios/modules/resume_module/model/item_resume_model.dart';
import 'package:portfolios/modules/resume_module/model/list_resume.dart';
import 'package:portfolios/modules/resume_module/widget/item_experience.dart';
import 'package:portfolios/shared/constants/constants.dart';

class ItemResume extends StatefulWidget {
  final String title;

  ItemResume(this.title);

  @override
  _ItemResumeState createState() => _ItemResumeState();
}

class _ItemResumeState extends State<ItemResume>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;

  @override
  void initState() {
    _animationController = AnimationController(
        vsync: this, duration: Duration(milliseconds: 2500));
    Timer(Duration(milliseconds: 200), () => _animationController.forward());
    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<ItemResumeModel> listExperience = ListResume.listExperience;
    List<ItemResumeModel> listEducation = ListResume.listEducation;
    List<ItemResumeModel> listSkills = ListResume.listSkills;
    List<ItemResumeModel> listReference = ListResume.listReference;
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Spacer(flex: 2),
        Expanded(
          flex: 2,
          child: Container(
            // alignment: Alignment.center,
            child: Text(
              widget.title,
              // textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black,
                fontSize: 26,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
        Spacer(flex: 1),
        Expanded(
          flex: 3,
          child: Container(
            child: Column(
              children: widget.title == Constants.experience
                  ? listExperience
                      .map(
                        (e) => ItemExperience(
                            e, _animationController, listExperience.indexOf(e)),
                      )
                      .toList()
                  : widget.title == Constants.education
                      ? listEducation
                          .map(
                            (e) => ItemExperience(e, _animationController,
                                listEducation.indexOf(e)),
                          )
                          .toList()
                      : widget.title == Constants.skill
                          ? listSkills
                              .map(
                                (e) => ItemExperience(e, _animationController,
                                    listSkills.indexOf(e)),
                              )
                              .toList()
                          : listReference
                              .map(
                                (e) => ItemExperience(e, _animationController,
                                    listReference.indexOf(e)),
                              )
                              .toList(),
            ),
          ),
        ),
        Spacer(flex: 1),
      ],
    );
  }
}
