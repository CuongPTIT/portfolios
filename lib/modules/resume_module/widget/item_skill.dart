import 'package:flutter/material.dart';
import 'package:portfolios/modules/resume_module/widget/bullet/bullet_list.dart';

class ItemSkill extends StatelessWidget {
  final String title;

  ItemSkill(this.title);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Spacer(flex: 2),
        Expanded(
          flex: 2,
          child: Container(
            // alignment: Alignment.center,
            child: Text(
              title,
              // textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black,
                fontSize: 26,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
        Spacer(flex: 1),
        Expanded(
          flex: 3,
          child: BulletList(
            strings: [],
          ),
        ),
        Spacer(flex: 1),
      ],
    );
  }
}
