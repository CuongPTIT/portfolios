import 'package:flutter/material.dart';
import 'package:portfolios/modules/resume_module/model/item_resume_model.dart';
import 'package:portfolios/widget/item_text.dart';

class ItemExperience extends StatelessWidget {
  final ItemResumeModel item;
  final AnimationController animationController;
  final int index;

  ItemExperience(this.item, this.animationController, this.index);

  @override
  Widget build(BuildContext context) {
    double _animationStart = 0.1 * index;
    double _animationEnd = _animationStart + 0.4;
    return SlideTransition(
      position: Tween<Offset>(begin: Offset(2, 0), end: Offset(0, 0)).animate(
        CurvedAnimation(
          parent: animationController,
          curve: Interval(_animationStart, _animationEnd, curve: Curves.ease),
        ),
      ),
      child: FadeTransition(
        opacity: animationController,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: 125,
              child: Text(
                item.time,
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
            ),
            SizedBox(
              width: 30,
            ),
            Expanded(
              child: ItemText(item.title, item.description),
            )
          ],
        ),
      ),
    );
  }
}
