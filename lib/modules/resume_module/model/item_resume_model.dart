class ItemResumeModel{

  String title;
  String time;
  String description;

  ItemResumeModel({required this.title, required this.time, required this.description});
}