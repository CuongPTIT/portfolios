import 'package:portfolios/modules/resume_module/model/item_resume_model.dart';
import 'package:portfolios/shared/constants/constants.dart';

class ListResume {
  static List<ItemResumeModel> listExperience = [
    ItemResumeModel(
        title: Constants.pirago,
        time: Constants.timePirago,
        description: Constants.desPirago),
    ItemResumeModel(
        title: Constants.vnpt,
        time: Constants.timeVNPT,
        description: Constants.desVNPT),
    ItemResumeModel(
        title: Constants.vccorp,
        time: Constants.timeVccorp,
        description: Constants.desVccorp),
  ];
  static List<ItemResumeModel> listEducation = [
    ItemResumeModel(
        title: Constants.ptit,
        time: Constants.timePTIT,
        description: Constants.desPTIT),
    ItemResumeModel(
        title: Constants.devpro,
        time: Constants.timeDevPro,
        description: Constants.desDevPro),
  ];
  static List<ItemResumeModel> listSkills = [
    ItemResumeModel(
        title: Constants.languages,
        time: Constants.timeBlank,
        description: Constants.desLanguages),
    ItemResumeModel(
        title: Constants.databases,
        time: Constants.timeBlank,
        description: Constants.desDB),
    ItemResumeModel(
        title: Constants.english,
        time: Constants.timeBlank,
        description: Constants.desEnglish),
    ItemResumeModel(
        title: Constants.otherSkills,
        time: Constants.timeBlank,
        description: Constants.desOtherSkills),
  ];
  static List<ItemResumeModel> listReference = [
    ItemResumeModel(
        title: Constants.personReference,
        time: Constants.timeBlank,
        description: Constants.desReference),
  ];
}
