import 'package:flutter/material.dart';
import 'package:portfolios/modules/resume_module/widget/item_resume.dart';
import 'package:portfolios/shared/constants/constants.dart';

class ResumeTab extends StatefulWidget {
  const ResumeTab({Key? key}) : super(key: key);

  @override
  _ResumeTabState createState() => _ResumeTabState();
}

class _ResumeTabState extends State<ResumeTab> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 100),
      child: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              ItemResume(Constants.experience),
              Divider(
                height: 100,
                thickness: 1,
                endIndent: 50,
                indent: 100,
              ),
              ItemResume(Constants.education),
              Divider(
                height: 50,
                thickness: 1,
                endIndent: 100,
                indent: 100,
              ),
              ItemResume(Constants.skill),
              Divider(
                height: 50,
                thickness: 1,
                endIndent: 100,
                indent: 100,
              ),
              ItemResume(Constants.reference),
            ],
          ),
        ),
      ),
    );
  }
}
