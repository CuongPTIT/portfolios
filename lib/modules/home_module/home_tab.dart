import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:portfolios/shared/constants/constants.dart';
import 'package:portfolios/shared/utils/tab_controller_handler.dart';
import 'package:portfolios/widget/circle_button.dart';

import '../../shared/utils/extension.dart';

class HomeTab extends StatefulWidget {
  @override
  _HomeTabState createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab> with SingleTickerProviderStateMixin {
  var sizeHeight;
  var sizeWidth;
  late AnimationController _animationController;

  @override
  void initState() {
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 10),
    );
    // _animationController.repeat();
    Timer(Duration(milliseconds: 1000), () => _animationController.repeat());
    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    sizeHeight = context.sizeHeight();
    sizeWidth = context.sizeWith();
    TabController? tabController =
        TabControllerHandler.of(context)?.tabController;
    return LayoutBuilder(
      builder: (context, constraints) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            avatar(),
            SizedBox(
              width: sizeWidth * 0.03,
            ),
            Container(
              width: sizeWidth * 0.45,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  name(),
                  SizedBox(
                    height: sizeHeight * 0.03,
                  ),
                  Wrap(
                    direction: Axis.horizontal,
                    crossAxisAlignment: WrapCrossAlignment.start,
                    children: [
                      CircleButton(
                        size: getImageButton(),
                        fontSize: getFontSize(false),
                        color: Colors.amberAccent,
                        title: Constants.resume,
                        onPress: () {
                          tabController?.animateTo(1);
                        },
                      ),
                      CircleButton(
                        size: getImageButton(),
                        fontSize: getFontSize(false),
                        color: Colors.redAccent,
                        title: Constants.project,
                        bottomMargin: sizeWidth * 0.03,
                        rightMargin: sizeWidth * 0.03,
                        leftMargin: sizeWidth * 0.03,
                        onPress: () {
                          tabController?.animateTo(2);
                        },
                      ),
                      CircleButton(
                        size: getImageButton(),
                        fontSize: getFontSize(false),
                        color: Colors.tealAccent,
                        title: Constants.contact,
                        onPress: () {
                          tabController?.animateTo(3);
                        },
                      ),
                    ],
                  )
                ],
              ),
            )
          ],
        );
      },
    );
  }

  Widget avatar() {
    return AnimatedBuilder(
      animation: _animationController.view,
      builder: (context, child) {
        return Transform.rotate(
          angle: _animationController.value * 2 * pi,
          child: child,
        );
      },
      child: Container(
        height: getImageSize(),
        width: getImageSize(),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(getImageSize() / 2),
          child: Image.asset(
            Constants.imgAvatar,
            fit: BoxFit.fill,
          ),
        ),
      ),
    );
  }

  Widget name() {
    return RichText(
      text: TextSpan(
        style: TextStyle(
          color: Colors.black,
          fontSize: getFontSize(true),
        ),
        children: [
          TextSpan(text: Constants.hello),
          TextSpan(
            text: Constants.surName,
            style: TextStyle(color: Colors.blue),
          ),
          TextSpan(text: Constants.chamThan),
        ],
      ),
    );
  }

  double getImageSize() {
    if (sizeWidth > 1600 && sizeHeight > 800) {
      return 400;
    } else if (sizeWidth > 1300 && sizeHeight > 600) {
      return 350;
    } else if (sizeWidth > 1000 && sizeHeight > 400) {
      return 300;
    } else {
      return 250;
    }
  }

  double getImageButton() {
    if (sizeWidth > 1600 && sizeHeight > 800) {
      return 150;
    } else if (sizeWidth > 1300 && sizeHeight > 600) {
      return 125;
    } else if (sizeWidth > 1000 && sizeHeight > 400) {
      return 100;
    } else {
      return 75;
    }
  }

  double getFontSize(bool isHeader) {
    double fontSize = sizeWidth > 1293 && sizeHeight > 550 ? 30 : 18;
    return isHeader ? fontSize * 2.0 : fontSize;
  }
}
