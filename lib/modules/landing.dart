import 'package:flutter/material.dart';
import 'package:portfolios/modules/contact_module/contact_tab.dart';
import 'package:portfolios/modules/home_module/home_tab.dart';
import 'package:portfolios/modules/project_module/project_tab.dart';
import 'package:portfolios/modules/resume_module/resume_tab.dart';
import 'package:portfolios/shared/constants/constants.dart';
import 'package:portfolios/shared/utils/tab_controller_handler.dart';
import 'package:portfolios/widget/tab_bar/content_view.dart';
import 'package:portfolios/widget/tab_bar/custom_tab.dart';
import 'package:portfolios/widget/tab_bar/custom_tab_bar.dart';
import 'package:portfolios/widget/footer/footer.dart';

class Landing extends StatefulWidget {
  const Landing({Key? key}) : super(key: key);

  @override
  _LandingState createState() => _LandingState();
}

class _LandingState extends State<Landing>
    with SingleTickerProviderStateMixin {
  late TabController tabController;
  List<ContentView> contentViews = [
    ContentView(
      tab: CustomTab(Constants.home),
      content: HomeTab(),
    ),
    ContentView(
      tab: CustomTab(Constants.resume),
      content: ResumeTab(),
    ),
    ContentView(
      tab: CustomTab(Constants.project),
      content: ProjectTab(),
    ),
    ContentView(
      tab: CustomTab(Constants.contact),
      content: ContactTab(),
    ),
  ];

  @override
  void initState() {
    super.initState();
    tabController = TabController(length: contentViews.length, vsync: this);
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Color(0xFFf3f3f3),
      body: LayoutBuilder(
        builder: (context, constraints) {
          return Column(
            children: [
              CustomTabBar(
                controller: tabController,
                tabs: contentViews.map((e) => e.tab).toList(),
              ),
              Expanded(
                child: Container(
                  // height: screenHeight * 0.85 - 100,
                  child: TabControllerHandler(
                    tabController: tabController,
                    child: TabBarView(
                      controller: tabController,
                      children: contentViews.map((e) => e.content).toList(),
                    ),
                  ),
                ),
              )
            ],
          );
        },
      ),
      persistentFooterButtons: [
        Footer(),
      ],
    );
  }
}
