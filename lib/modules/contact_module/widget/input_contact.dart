import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:portfolios/widget/text_input.dart';

import '../../../../shared/constants/colors.dart';

class InputContact extends StatelessWidget {
  final String titleText;
  final TextEditingController controller;
  final int maxLine;
  final Color colorText;
  final int? length;
  final double width;
  final double circular;
  final Function(String) validator;

  InputContact(this.titleText,
      {required this.controller,
      this.length,
      this.circular = 25,
      this.maxLine = 1,
      this.width = 1,
      this.colorText = Palette.black,
      required this.validator});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          alignment: Alignment(-.95, 0.0),
          child: Text(
            titleText,
            style: TextStyle(color: Colors.black),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 3, bottom: 5.0, right: 3),
          child: TextInputBorder(
            length: length,
            widthSize: width,
            maxLine: maxLine,
            circular: circular,
            controller: controller,
            validator: validator,
          ),
        ),
      ],
    );
  }
}
