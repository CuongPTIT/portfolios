import 'package:flutter/material.dart';
import 'package:mailto/mailto.dart';
import 'package:portfolios/modules/contact_module/widget/input_contact.dart';
import 'package:portfolios/shared/constants/constants.dart';
import 'package:portfolios/widget/circle_button.dart';
import 'package:portfolios/widget/item_text.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../shared/utils/extension.dart';

class ContactTab extends StatefulWidget {
  const ContactTab({Key? key}) : super(key: key);

  @override
  _ContactTabState createState() => _ContactTabState();
}

class _ContactTabState extends State<ContactTab> {
  TextEditingController hoController = TextEditingController();
  TextEditingController tenController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController subjectController = TextEditingController();
  TextEditingController mesController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 100),
      child: SingleChildScrollView(
        child: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Spacer(flex: 2),
              Expanded(
                flex: 2,
                child: Padding(
                  padding: EdgeInsets.all((context.sizeWith() * 0.03)),
                  child: Container(
                    // alignment: Alignment.center,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ItemText(Constants.textName, Constants.fullName),
                        ItemText(Constants.textBirthday, Constants.birthday),
                        ItemText(Constants.textAddress, Constants.address),
                        ItemText(Constants.textCountry, Constants.country),
                        ItemText(Constants.email, Constants.mail),
                        ItemText(Constants.phone, Constants.numberPhone),
                      ],
                    ),
                  ),
                ),
              ),
              Spacer(flex: 1),
              Expanded(
                flex: 3,
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: InputContact(
                              Constants.ho,
                              controller: hoController,
                              length: 255,
                              validator: (val) {},
                            ),
                          ),
                          Expanded(
                            child: InputContact(
                              Constants.textName,
                              controller: tenController,
                              length: 255,
                              validator: (val) {},
                            ),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: InputContact(
                              Constants.email,
                              controller: emailController,
                              length: 255,
                              validator: (val) {},
                            ),
                          ),
                          Expanded(
                            child: InputContact(
                              Constants.subject,
                              controller: subjectController,
                              length: 255,
                              validator: (val) {},
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: InputContact(
                              Constants.content,
                              controller: mesController,
                              length: 255,
                              circular: 10,
                              maxLine: 15,
                              validator: (val) {},
                            ),
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          CircleButton(
                            size: 150,
                            fontSize: 25,
                            color: Colors.amberAccent,
                            title: Constants.send,
                            onPress: () {
                              launchMailto();
                            },
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
              Spacer(flex: 1),
            ],
          ),
        ),
      ),
    );
  }

  launchMailto() async {
    final mailtoLink = Mailto(
      to: [Constants.mail],
      subject: subjectController.text.trim(),
      body: mesController.text.trim(),
    );
    // Convert the Mailto instance into a string.
    // Use either Dart's string interpolation
    // or the toString() method.
    await launch('$mailtoLink');
  }
}
