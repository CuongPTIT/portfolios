import 'dart:async';

import 'package:flutter/material.dart';
import 'package:portfolios/modules/project_module/model/list_project.dart';
import 'package:portfolios/modules/project_module/model/project_model.dart';
import 'package:portfolios/modules/project_module/widget/item_project.dart';

class ProjectTab extends StatefulWidget {
  const ProjectTab({Key? key}) : super(key: key);

  @override
  _ProjectTabState createState() => _ProjectTabState();
}

class _ProjectTabState extends State<ProjectTab>
    with SingleTickerProviderStateMixin {
  List<ProjectModel> listProject = ListProject.listProject;
  late AnimationController _animationController;

  @override
  void initState() {
    _animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 2500));
    Timer(Duration(milliseconds: 200), () => _animationController.forward());
    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 100),
      child: SingleChildScrollView(
        child: Container(
          child: Column(
            children: listProject.map((e) {
              return Column(
                children: [
                  ItemProject(
                    e,
                    _animationController,
                    listProject.indexOf(e),
                  ),
                  Divider(
                    height: 50,
                    thickness: 1,
                    endIndent: 100,
                    indent: 100,
                  ),
                ],
              );
            }).toList(),
          ),
        ),
      ),
    );
  }
}
