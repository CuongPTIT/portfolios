class ProjectModel {
  String name;
  String description;
  String teamSize;
  String responsibility;
  String technology;
  String link;
  String image;

  ProjectModel({
    required this.name,
    required this.description,
    required this.teamSize,
    required this.responsibility,
    required this.technology,
    required this.link,
    required this.image,
  });
}
