import 'package:portfolios/modules/project_module/model/project_model.dart';
import 'package:portfolios/shared/constants/constants.dart';

class ListProject {
  static List<ProjectModel> listProject = [
    ProjectModel(
      name: Constants.diy,
      description: Constants.desDIY,
      teamSize: Constants.sizeDIY,
      responsibility: Constants.resDIY,
      technology: Constants.techDIY,
      link: Constants.linkDIY,
      image: Constants.imgDIY,
    ),
    ProjectModel(
      name: Constants.piranet,
      description: Constants.desPi,
      teamSize: Constants.sizePi,
      responsibility: Constants.resPi,
      technology: Constants.techPi,
      link: Constants.linkPi,
      image: Constants.imgPi,
    ),
    ProjectModel(
      name: Constants.tatsuno,
      description: Constants.desTat,
      teamSize: Constants.sizeTat,
      responsibility: Constants.resTat,
      technology: Constants.techTat,
      link: Constants.linkTat,
      image: Constants.imgTat,
    ),
    ProjectModel(
      name: Constants.dietto,
      description: Constants.desDiet,
      teamSize: Constants.sizeDiet,
      responsibility: Constants.resDiet,
      technology: Constants.techDiet,
      link: Constants.linkDiet,
      image: Constants.imgDiet,
    ),
    ProjectModel(
      name: Constants.vanMau,
      description: Constants.desVM,
      teamSize: Constants.sizePersonal,
      responsibility: Constants.resVM,
      technology: Constants.techVM,
      link: Constants.linkVM,
      image: Constants.imgVM,
    ),
    ProjectModel(
      name: Constants.weatherForecast,
      description: Constants.desWeather,
      teamSize: Constants.sizePersonal,
      responsibility: Constants.resWeather,
      technology: Constants.techWeather,
      link: Constants.linkWeather,
      image: Constants.imgWeather,
    ),
  ];
}
