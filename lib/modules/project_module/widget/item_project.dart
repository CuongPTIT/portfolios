import 'package:flutter/material.dart';
import 'package:portfolios/modules/project_module/model/project_model.dart';
import 'package:portfolios/shared/constants/constants.dart';

import '../../../shared/utils/extension.dart';

class ItemProject extends StatelessWidget {
  final ProjectModel item;
  final AnimationController animationController;
  final int index;

  ItemProject(this.item, this.animationController, this.index);

  @override
  Widget build(BuildContext context) {
    double _animationStart = 0.1 * index;
    double _animationEnd = _animationStart + 0.4;
    return SlideTransition(
      position: Tween<Offset>(
        begin: const Offset(0, 2),
        end: Offset(0, 0),
      ).animate(CurvedAnimation(
        parent: animationController,
        curve: Interval(_animationStart, _animationEnd, curve: Curves.ease),
      )),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Spacer(flex: 2),
          Expanded(
            flex: 2,
            child: Padding(
              padding: EdgeInsets.all((context.sizeWith() * 0.03)),
              child: Container(
                // alignment: Alignment.center,
                child: Image.asset(
                  item.image,
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ),
          Spacer(flex: 1),
          Expanded(
            flex: 3,
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  RichText(
                    text: TextSpan(
                      style: TextStyle(
                        color: Colors.black,
                      ),
                      children: [
                        TextSpan(
                          text: Constants.nameProject,
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.bold),
                        ),
                        TextSpan(
                          text: item.name,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: context.sizeHeight() * 0.05,
                  ),
                  RichText(
                    text: TextSpan(
                      style: TextStyle(
                        color: Colors.black,
                      ),
                      children: [
                        TextSpan(
                          text: Constants.description,
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.bold),
                        ),
                        TextSpan(
                          text: item.description,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: context.sizeHeight() * 0.05,
                  ),
                  RichText(
                    text: TextSpan(
                      style: TextStyle(
                        color: Colors.black,
                      ),
                      children: [
                        TextSpan(
                          text: Constants.teamSize,
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.bold),
                        ),
                        TextSpan(
                          text: item.teamSize,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: context.sizeHeight() * 0.05,
                  ),
                  RichText(
                    text: TextSpan(
                      style: TextStyle(
                        color: Colors.black,
                      ),
                      children: [
                        TextSpan(
                          text: Constants.responsibility,
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.bold),
                        ),
                        TextSpan(
                          text: item.responsibility,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: context.sizeHeight() * 0.05,
                  ),
                  RichText(
                    text: TextSpan(
                      style: TextStyle(
                        color: Colors.black,
                      ),
                      children: [
                        TextSpan(
                          text: Constants.technology,
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.bold),
                        ),
                        TextSpan(
                          text: item.technology,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: context.sizeHeight() * 0.05,
                  ),
                ],
              ),
            ),
          ),
          Spacer(flex: 1),
        ],
      ),
    );
  }
}
