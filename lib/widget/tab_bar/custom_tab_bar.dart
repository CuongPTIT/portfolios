import 'package:flutter/material.dart';
import '../../shared/utils/extension.dart';
class CustomTabBar extends StatelessWidget {
  final TabController controller;
  final List<Widget> tabs;

  CustomTabBar({required this.controller, required this.tabs});

  @override
  Widget build(BuildContext context) {
    var tabBarScaling = context.sizeWith() > 1400
        ? 0.25
        : context.sizeWith() > 1100
            ? 0.5
            : 0.6;
    return Container(
      width: context.sizeWith() * tabBarScaling,
      child: TabBar(
        controller: controller,
        tabs: tabs,
      ),
    );
  }
}
