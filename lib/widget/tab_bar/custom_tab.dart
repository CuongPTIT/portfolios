import 'package:flutter/material.dart';

class CustomTab extends StatelessWidget {
  final String title;

  CustomTab(this.title);

  @override
  Widget build(BuildContext context) {
    return Tab(
      child: Text(title, style: TextStyle(fontSize: 17, color: Colors.black),),
    );
  }
}
