import 'package:flutter/material.dart';

class ItemText extends StatelessWidget {
  final String title;
  final String description;

  ItemText(this.title, this.description);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
            fontSize: 18,
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          description,
          textAlign: TextAlign.justify,
          overflow: TextOverflow.visible,
          style: TextStyle(color: Colors.black),
        ),
        SizedBox(
          height: 30,
        ),
      ],
    );
  }
}
