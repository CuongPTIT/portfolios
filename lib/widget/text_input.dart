import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../shared/constants/colors.dart';
import '../../shared/constants/constants.dart';
import '../../shared/utils/extension.dart';

class TextInputBorder extends StatelessWidget {
  final String? hintText;
  final TextEditingController? controller;
  final Function(String)? validator;
  final Function(String)? onchange;
  final Function()? onTap;
  final IconData? prefixIcon;
  final Widget? suffixIcon;
  final bool? check;
  final int maxLine;
  final int? length;
  final double widthSize;
  final FocusNode? focusNode;
  final bool enabled;
  final String? label;
  final double circular;

  TextInputBorder({
    this.hintText,
    this.controller,
    this.validator,
    this.onchange,
    this.onTap,
    this.prefixIcon,
    this.suffixIcon,
    this.check,
    this.widthSize = 1,
    this.maxLine = 1,
    this.length,
    this.circular = 25,
    this.focusNode,
    this.enabled = true,
    this.label,
  });

  @override
  Widget build(BuildContext context) {
    var width = context.sizeWith() * widthSize;
    return Container(
      width: width,
      margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 0),
      child: TextFormField(
        focusNode: focusNode,
        controller: controller,
        maxLines: maxLine,
        maxLength: length,
        style: TextStyle(
          fontSize: Constants.tsNormal,
        ),
        onChanged: onchange,
        onTap: onTap,
        decoration: InputDecoration(
          counterText: "",
          filled: true,
          errorMaxLines: 3,
          labelText: label,
          suffixIcon: suffixIcon,
          prefixIcon: prefixIcon != null
              ? Icon(
                  prefixIcon,
                  color: Palette.colorAppbar,
                  size: 30,
                )
              : null,
          fillColor: Color(0xFFf3f3f3),
          contentPadding:
              EdgeInsets.only(left: 10.0, bottom: 5.0, top: 5.0),
          hintText: hintText,
          hoverColor: Colors.white,
          hintStyle: TextStyle(color: Palette.grayBorder),
          errorStyle: TextStyle(
            fontSize: Constants.tsNormal,
            fontWeight: FontWeight.w900,
            color: Palette.red,
          ),
          enabled: enabled,
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(circular),
            borderSide: BorderSide(color: Palette.grayBorder, width: 1),
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(circular),
            borderSide: BorderSide(color: Palette.grayBorder, width: 1),
          ),
          disabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(circular),
            borderSide: BorderSide(color: Palette.grayBorder, width: 1),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(circular),
            borderSide: BorderSide(color: Palette.grayBorderFocused, width: 1),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(circular),
            borderSide: BorderSide(color: Palette.red, width: 0.8),
          ),
        ),
        validator: (val) {
          if (validator == null) {
            return null;
          }
          return validator!(val!);
        },
      ),
    );
  }
}
