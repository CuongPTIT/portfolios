import 'package:flutter/material.dart';
import 'package:hovering/hovering.dart';

class CircleButton extends StatelessWidget {
  final double size;
  final double fontSize;
  final String title;
  final Color color;
  final double leftMargin;
  final double rightMargin;
  final double bottomMargin;
  final double topMargin;
  final Function? onPress;

  CircleButton(
      {required this.size,
      required this.fontSize,
      required this.color,
      this.leftMargin = 0,
      this.rightMargin = 0,
      this.bottomMargin = 0,
      this.topMargin = 0,
      this.onPress,
      required this.title});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        onPress!();
      },
      child: HoverContainer(
        cursor: SystemMouseCursors.click,
        margin: EdgeInsets.only(
            left: leftMargin,
            right: rightMargin,
            bottom: bottomMargin,
            top: topMargin),
        height: size,
        width: size,
        hoverDecoration: BoxDecoration(
          color: Colors.white70,
          borderRadius: BorderRadius.circular(size / 2),
          border: Border.all(color: Colors.black,width: 1),
        ),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(size / 2),
          border: Border.all(color: Colors.black),
        ),
        alignment: Alignment.center,
        child: Text(
          title,
          style: TextStyle(
            color: Colors.black,
            fontSize: fontSize,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
