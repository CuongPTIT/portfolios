import 'package:flutter/material.dart';
import 'package:portfolios/shared/constants/constants.dart';
import 'package:url_launcher/url_launcher.dart';

import 'item_footer.dart';

class Footer extends StatelessWidget {
  const Footer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      alignment: Alignment.center,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ItemFooter(
            title: Constants.phone,
            child: Text(Constants.numberPhone),
          ),
          ItemFooter(
            title: Constants.email,
            child: Text(Constants.mail),
          ),
          ItemFooter(
            title: Constants.followMe,
            child: Row(
              children: [
                IconButton(
                  onPressed: () {
                    launcherUrl(Constants.urlFb);
                  },
                  icon: Image.asset(
                    Constants.icFb,
                    fit: BoxFit.contain,
                  ),
                ),
                SizedBox(width: 10,),
                IconButton(
                  onPressed: () {
                    launcherUrl(Constants.urlBlog);
                  },
                  icon: Image.asset(
                    Constants.icBlog,
                    fit: BoxFit.contain,
                  ),
                ),
              ],
            ),
          ),
          ItemFooter(
            title:  Constants.copyright,
            child: Text(Constants.createdBy),
          ),
        ],
      ),
    );
  }

  launcherUrl(String url) async {
    await canLaunch(url) ? await launch(url) : throw 'Can not launch $url';
  }
}
